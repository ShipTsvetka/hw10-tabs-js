"use strict";

const tabContainer = document.querySelector('.tabs');

let count = 0;

for (let elem of tabContainer.children) {
    elem.dataset.id = count++;
};

const textTabsContainer = document.querySelector('.tabs-content');

let countTextTabs = 0;

for (let elem of textTabsContainer.children) {
    elem.dataset.id = countTextTabs++;
};

tabContainer.addEventListener('click', (event) => {
    for (let elem of tabContainer.children) {
        elem.classList.remove('active');
    };

    event.target.className += ' active';

    for (let elem of textTabsContainer.children) {
        elem.classList.remove('shown');

        if (event.target.dataset.id === elem.dataset.id) {
            elem.className = 'shown';
        } else {
            elem.className = 'hidden';
        };
    };
});
